package xyz.spacefox.rateme;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import org.testpackage.test_sdk.android.testlib.API;
import org.testpackage.test_sdk.android.testlib.interfaces.SuccessCallback;

import xyz.spacefox.rateme.utils.Database;

/**
 * Refresh activity: allow user to refresh database and data from api emulator if needed.
 * Refreshing starts after user push "Refresh" button. After data refreshing is done - main activity
 * starts.
 */
public class RefreshActivity extends AppCompatActivity {

    // Activity toolbar
    private Toolbar mToolbar;

    // "Refresh" button
    private Button mRefreshBtn;

    // ProgressBar to display while refreshing
    private ProgressBar mRefreshingProgress;

    // Database helper instance
    private Database mdB;

    // Tag to mark logs
    private final String LOG_TAG = RefreshActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refresh);

        initToolbar();

        // Initialize and ope database
        mdB = new Database(RefreshActivity.this);
        mdB.open();

        // Locate activity views
        mRefreshBtn = (Button) findViewById(R.id.refresh_btn);
        mRefreshingProgress = (ProgressBar) findViewById(R.id.refreshing_progress);

        // Setting refresh button onClick Listener
        mRefreshBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Display ProgressBar and disable refresh button while refreshing
                mRefreshingProgress.setVisibility(View.VISIBLE);
                mRefreshBtn.setEnabled(false);

                // Start refreshing task
                refreshUsers();

            }
        });


    }

    @Override
    protected void onDestroy() {
        mdB.close();
        super.onDestroy();
    }

    /**
     * Refresh users data in API library using async task.
     */
    private void refreshUsers(){
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {

                Log.d(LOG_TAG, "Refresh started");

                // Clear existing tables from a local database
                mdB.clearUsers();
                mdB.clearLikes();
                API.INSTANCE.refreshPersons(new SuccessCallback() {
                    @Override
                    public void onSuccess() {
                        Log.d(LOG_TAG, "Persons refreshed");

                        // Start Main activity after task done
                        Intent startMain = new Intent(RefreshActivity.this, MainActivity.class);
                        startActivity(startMain);
                        finish();

                    }
                });
            }
        });
    }

    /** Initializes the toolbar. */
    private void initToolbar(){
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
    }
}
