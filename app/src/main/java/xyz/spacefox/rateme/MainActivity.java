package xyz.spacefox.rateme;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import xyz.spacefox.rateme.adapters.ViewPagerAdapter;
import xyz.spacefox.rateme.services.ApiUpdateService;
import xyz.spacefox.rateme.utils.Consts;
import xyz.spacefox.rateme.utils.Database;

/**
 * Main Activity: contains two tabs.
 *
 * Persons tab contain feed with active persons user can like or dislike.
 * Map tab shows active persons on map as custom markers.
 *
 * Implements LoaderCallbacks to get connection with database.
 */

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    // Activity toolbar
    private Toolbar mToolbar;

    // Database helper instance
    private Database mDb;

    // Main loader
    private Loader mLoader;

    // Viewpager and Tab layout to host two tabs
    private ViewPager mViewPager;
    private TabLayout mTabLayout;

    private BroadcastReceiver mUpdateReceiver;

    // Map and persons fragments
    private PersonsFeedFragment mPersonsFragment;
    private PersonsMapFragment mMapFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Initialize the toolbar
        initToolbar();

        // Initialize and open database connection
        mDb = new Database(this);
        mDb.open();
        // Clear early loaded users
        if (savedInstanceState==null) {
            mDb.clearUsers();
        }

        // Initialize fragments
        mPersonsFragment = PersonsFeedFragment.newInstance();
        mMapFragment = PersonsMapFragment.newInstance();

        // Initialize main loader
        mLoader = getSupportLoaderManager().initLoader(Consts.PERSONS_FEED_LOADER_ID, null, this);

        // Initialize broadcast receiver to reload data from database
        mUpdateReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(mLoader != null) {
                    mLoader.forceLoad();
                }
            }
        };

        // Locate and init viewpager
        mViewPager = (ViewPager) findViewById(R.id.main_viewpager);
        initViewPager();

        // Locate and init tab layout
        mTabLayout = (TabLayout) findViewById(R.id.main_tabs);
        mTabLayout.setupWithViewPager(mViewPager);

        // Starting update service
        startService(new Intent(MainActivity.this, ApiUpdateService.class));

    }

    @Override
    protected void onStart() {
        super.onStart();

        // Register receiver to activity
        LocalBroadcastManager.getInstance(MainActivity.this)
                .registerReceiver(mUpdateReceiver, new IntentFilter(Consts.UPDATE_BROADCAST_TAG));
    }

    @Override
    protected void onStop() {
        super.onStop();

        // Unregister update receiver
        LocalBroadcastManager.getInstance(MainActivity.this).unregisterReceiver(mUpdateReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Close database and destroy loader
        mDb.close();
        getLoaderManager().destroyLoader(Consts.PERSONS_FEED_LOADER_ID);

        // Stop update service when activity is destroyed.
        stopService(new Intent(MainActivity.this, ApiUpdateService.class));
    }

    /** Initializes the toolbar. */
    private void initToolbar(){
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(getString(R.string.app_name));
        setSupportActionBar(mToolbar);
    }

    /** Initializes the ViewPager. */
    private void initViewPager(){
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(mPersonsFragment, getString(R.string.persons_tab));
        adapter.addFragment(mMapFragment, getString(R.string.map_tab));
        mViewPager.setAdapter(adapter);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new MyCursorLoader(this, mDb);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        // Manage data update on fragments level
        mMapFragment.updateUsers(data);
        mPersonsFragment.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    /**
     * Subclass of {@link android.content.CursorLoader} which provides loader associated
     * with application database's implementation.
     */
    static class MyCursorLoader extends CursorLoader {

        Database db;

        public MyCursorLoader(Context context, Database db) {
            super(context);
            this.db = db;
        }

        @Override
        public Cursor loadInBackground() {
            return db.getActiveUsers();
        }

    }

}
