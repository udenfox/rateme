package xyz.spacefox.rateme.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import xyz.spacefox.rateme.api.model.User;

/**
 * Singleton class that implements Json Parser using GSON.
 */
public class ApiParser {

    private static ApiParser sInstance = new ApiParser();
    private Gson mGson;

    private ApiParser() {
        mGson = new GsonBuilder().create();
    }

    /**
     * @return parser instance
     */
    public static ApiParser getInstance(){
        return sInstance;
    }

    /**
     * Parses and return list of users from a JSON string.
     * @param userListJson Json string that contain list of users
     * @return List of {@link xyz.spacefox.rateme.api.model.User}
     */
    public List<User> parseUserList(String userListJson){
        Type usersList = new TypeToken<ArrayList<User>>(){}.getType();
        return  mGson.fromJson(userListJson, usersList);

    }

    /**
     * Parses and return user from a JSON string.
     * @param userJson Json string that contain user
     * @return {@link xyz.spacefox.rateme.api.model.User} instance
     */
    public User parseUser(String userJson){
        return mGson.fromJson(userJson, User.class);
    }
}
