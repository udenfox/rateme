package xyz.spacefox.rateme.api.model;

import com.google.gson.annotations.SerializedName;

/**
 * Model class that represents user object provided by API.
 */
public class User {

    @SerializedName("id")
    private int mId;

    @SerializedName("location")
    private String mLocation;

    @SerializedName("status")
    private String mStatus;

    @SerializedName("photo")
    private String mPhoto;

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        this.mId = id;
    }

    public String getLocation() {
        return mLocation;
    }

    public void setLocation(String location) {
        this.mLocation = location;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        this.mStatus = status;
    }

    public String getPhoto() {
        return mPhoto;
    }

    public void setPhoto(String photo) {
        this.mPhoto = photo;
    }

    @Override
    public int hashCode() {
        int prime = 128;
        return  getId() + prime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null)
            return false;
        if (getClass() != o.getClass())
            return false;
        User other = (User) o;
        if (getId() != other.getId()){
            return false;
        }
        return true;
    }
}
