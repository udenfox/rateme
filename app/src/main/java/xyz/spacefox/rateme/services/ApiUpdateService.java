package xyz.spacefox.rateme.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import org.testpackage.test_sdk.android.testlib.API;
import org.testpackage.test_sdk.android.testlib.services.UpdateService;

import xyz.spacefox.rateme.R;
import xyz.spacefox.rateme.api.ApiParser;
import xyz.spacefox.rateme.api.model.User;
import xyz.spacefox.rateme.utils.Consts;
import xyz.spacefox.rateme.utils.Database;

/**
 * Service that provide handling updates provided by API.
 * Sends system notifications when users matched or some user has "removed" status.
 * All actual data comes to local database.
 * Service can notify about each data update through broadcast intent
 */
public class ApiUpdateService extends Service {

    // Database helper instance.
    private Database mDb;

    // Notification manager to notify user about actions
    private NotificationManager mNotifyMgr;

    private final String LOG_TAG = ApiUpdateService.class.getName();

    @Override
    public void onCreate() {
        super.onCreate();

        // Initialize and open database
        mDb = new Database(ApiUpdateService.this);
        mDb.open();

        // Initialize notification manager
        mNotifyMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.d(LOG_TAG, "Service started");

        // Start listening to API updates
        API.INSTANCE.subscribeUpdates(new UpdateService.UpdateServiceListener() {
            @Override
            public void onChanges(String person) {

                // Store two user instances: updated user and user that already in database
                User updatedUser = ApiParser.getInstance().parseUser(person);
                User currentUser = mDb.getUserById(updatedUser.getId());

                // update data only if updated user is already loaded into local storage
                if (currentUser != null) {
                    // Notify user if user changed his status
                    if (!updatedUser.getStatus().equals(currentUser.getStatus())){
                        switch (updatedUser.getStatus()) {

                            case Consts.STATUS_REMOVED:
                                notifyRemoved();
                                break;

                            case Consts.STATUS_LIKE:
                                if(mDb.getLikeStatus(updatedUser.getId())) {
                                    notifyMatch();
                                }
                                break;

                        }
                    }

                    // Save actual data to the database and notify data changed
                    mDb.addUser(updatedUser);
                    sendUpdateBroadcast();
                }

            }
        });

        return START_NOT_STICKY;

    }

    @Override
    public void onDestroy() {

        // Unsubscribe from API updates and close database after service destroyed
        API.INSTANCE.unSubscribeUpdates();
        mDb.close();
        super.onDestroy();
    }

    /**
     * Creates and show system notification about user's removed status.
     */
    private void notifyRemoved(){
        Notification removedNotify = new NotificationCompat.Builder(this)
                .setContentText(getString(R.string.notification_removed_text))
                .setContentTitle(getString(R.string.app_name))
                .setSmallIcon(R.drawable.ic_notification_removed)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                .build();
        mNotifyMgr.notify(Consts.REMOVE_NOTIFY_ID, removedNotify);
    }

    /**
     * Creates and show system notification about users match.
     */
    private void notifyMatch(){
        Notification matchNotify = new NotificationCompat.Builder(this)
                .setContentText(getString(R.string.notification_match_text))
                .setContentTitle(getString(R.string.app_name))
                .setSmallIcon(R.drawable.ic_notification_match)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                .setDefaults(Notification.DEFAULT_ALL)
                .build();
        mNotifyMgr.notify(Consts.MATCH_NOTIFY_ID, matchNotify);
    }

    /**
     * Sends broadcast to notify about data update
     */
    private void sendUpdateBroadcast(){
        Intent broadcastIntent = new Intent(Consts.UPDATE_BROADCAST_TAG);
        LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
    }

}


