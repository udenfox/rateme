package xyz.spacefox.rateme;


import android.Manifest;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import xyz.spacefox.rateme.api.model.User;
import xyz.spacefox.rateme.utils.Consts;
import xyz.spacefox.rateme.utils.Utils;

/**
 * PersonsMapFragment: {@link com.google.android.gms.maps.SupportMapFragment} that displays active
 * persons on map using custom markers (user photos)
 */
public class PersonsMapFragment extends SupportMapFragment implements OnMapReadyCallback {

    // Map instance
    private GoogleMap mMap;

    // Map to store updated user list
    private HashMap<Integer, User> mUserList;

    // Map to store existing markers on map
    private HashMap<Integer, Marker> mMarkers;

    // Set with Targets for Picasso proper image download
    private Set<PersonMarkerTarget> mPersonsTargets;
    private int markerSidePx;


    public PersonsMapFragment() {
        // Required empty public constructor
    }

    public static PersonsMapFragment newInstance() {
        PersonsMapFragment fragment = new PersonsMapFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPersonsTargets = new HashSet<>();
        mMarkers = new HashMap<>();
        getMapAsync(this);
        markerSidePx = getResources().getDimensionPixelSize(R.dimen.map_marker_side);

    }

    /**
     * Load users from database using cursor and sets markers on the map.
     * @param c cursor to load data from. Pass null if only markers update needed.
     */
    public void updateUsers(Cursor c) {

        if (c != null) {
            mUserList = Utils.getUsersAsList(c);
        }

        if (mMap != null && mUserList != null){

            PersonMarkerTarget pt;

            // Remove redundant markers if needed
            if(mUserList.size() != mMarkers.size()){
                for (Integer id : mMarkers.keySet()) {
                    if (mUserList.get(id) == null) {
                        mMarkers.get(id).remove();
                    }
                }
            }


            for (Map.Entry<Integer, User> userEntry : mUserList.entrySet()) {

                // If user marker not in the map yet - add it and load picture for it
                if (mMarkers.get(userEntry.getValue().getId()) == null) {
                    MarkerOptions mOptions = new MarkerOptions()
                            .position(Utils.getLatLngFromString(userEntry.getValue().getLocation()))
                            .anchor(Consts.MAP_MARKER_ANCHOR_U_V, Consts.MAP_MARKER_ANCHOR_U_V);
                    Marker m = mMap.addMarker(mOptions);
                    mMarkers.put(userEntry.getValue().getId(), m);
                    pt = new PersonMarkerTarget(m);
                    mPersonsTargets.add(pt);
                    Picasso.with(getActivity())
                            .load(userEntry.getValue().getPhoto())
                            .resize(markerSidePx, markerSidePx)
                            .into(pt);
                } else {

                    // If marker already populated - just change it location.
                    Marker m =  mMarkers.get(userEntry.getValue().getId());
                    m.setPosition(Utils.getLatLngFromString(userEntry.getValue().getLocation()));
                }


            }
        }


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        // Check needed by Google Map Api
        if (ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        googleMap.setMyLocationEnabled(true);

        // Move camera to Lisbon just for testing simplify. Can be changed to user current location.
        googleMap.moveCamera(CameraUpdateFactory.
                newLatLngZoom(new LatLng(38.736946, -9.142685), 12.0f));
        mMap = googleMap;

        // Just update map
        updateUsers(null);

    }

    /**
     * Class that creates targets for each map marker to load pictures in using Picasso.
     */
    class PersonMarkerTarget implements Target {
        private Marker m;

        public PersonMarkerTarget(Marker m) { this.m = m; }

        @Override public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {

            try {
                m.setIcon(BitmapDescriptorFactory.fromBitmap(bitmap));
            } catch (IllegalArgumentException e){

            }


            mPersonsTargets.remove(this);
        }

        @Override public void onBitmapFailed(Drawable errorDrawable) {
            mPersonsTargets.remove(this);
        }

        @Override public void onPrepareLoad(Drawable placeHolderDrawable) {

        }
    }
}
