package xyz.spacefox.rateme;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import org.testpackage.test_sdk.android.testlib.API;

import xyz.spacefox.rateme.utils.Consts;

/**
 * Splash activity: shows splash screen to user while initializing API emulator.
 * After initialization starts activity depends on database condition: if database have entries with
 * active users - Main activity will be started, if not - Refresh activity started instead.
 */
public class SplashActivity extends AppCompatActivity {

    // Shared preferences instance to detect application first start
    private SharedPreferences mPrefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPrefs = getSharedPreferences(Consts.PREFS_NAME, MODE_PRIVATE);

        // Initialize API emulation library
        API.INSTANCE.init(getApplicationContext());

        // Start first run check and activity change process
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                Intent startIntent;
                if (mPrefs.getBoolean(Consts.PREFS_FIRSTRUN, true)) {
                    startIntent = new Intent(SplashActivity.this, RefreshActivity.class);
                    mPrefs.edit().putBoolean(Consts.PREFS_FIRSTRUN, false).apply();
                } else {
                    startIntent = new Intent(SplashActivity.this, MainActivity.class);
                }

                startActivity(startIntent);
                finish();

            }
        });
    }
}
