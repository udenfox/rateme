package xyz.spacefox.rateme;


import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.testpackage.test_sdk.android.testlib.API;
import org.testpackage.test_sdk.android.testlib.interfaces.PersonsExtendedCallback;

import xyz.spacefox.rateme.adapters.PersonsCursorAdapter;
import xyz.spacefox.rateme.api.ApiParser;
import xyz.spacefox.rateme.interfaces.OnUserRateListener;
import xyz.spacefox.rateme.utils.Consts;
import xyz.spacefox.rateme.utils.Database;


/**
 * PersonsFeedFragment: fragment contains list view that represents feed with persons photos.
 * User can like or dislike person from list by pressing relevant button on every photo.
 */
public class PersonsFeedFragment extends Fragment {

    // Database helper instance
    private Database mDb;

    // Adapter for list view
    private PersonsCursorAdapter mPersonsAdapter;

    // List that represents feed
    private ListView mPersonsFeedList;

    // Progressbar to show refreshing state
    private ProgressBar mRefreshBar;

    // Empty view for a list
    private TextView mListEmptyView;

    // Number of page to load from API
    private int mPageToLoad;

    // Total pages and users counters
    private int mTotalPages;
    private int mTotalUsers;

    // Refreshing state indicator
    private Boolean mIsRefreshing = false;

    // Tag to mark logs
    private final String LOG_TAG = PersonsFeedFragment.class.getName();

    public PersonsFeedFragment() {
        // Required empty public constructor
    }

    /**
     * Create a new instance of NewsFeedFragment.
     */
    public static PersonsFeedFragment newInstance() {
        PersonsFeedFragment fragment = new PersonsFeedFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Initialize and open database connection
        mDb = new Database(getActivity());
        mDb.open();

        // Hardcoded values. In real project number of pages will be provided from API.
        mTotalUsers = Consts.USERS_TOTAL;
        mTotalPages = mTotalUsers / Consts.USERS_PER_PAGE;

        if (savedInstanceState != null) {
            mPageToLoad = savedInstanceState.getInt(Consts.SAVED_STATE_PAGE_TO_LOAD);
        }

        // Initialize list adapter
        mPersonsAdapter = new PersonsCursorAdapter(getActivity(), null, true);

        // Set listener to catch "like" or "dislike" buttons pressed actions from adapter
        mPersonsAdapter.setOnRateListener(new OnUserRateListener() {
            @Override
            public void onLike(int userId) {

                // Change user status and start match screen if needed
                mDb.setLikeStatus(userId, true);
                if (mDb.getUserById(userId).getStatus().equals(Consts.STATUS_LIKE)) {
                    performMatch(userId);
                }
                forceReload();

            }

            @Override
            public void onDislike(int userId) {

                // Change user status
                mDb.setLikeStatus(userId, false);
                forceReload();
            }
        });


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_persons_feed, container, false);

        // Initialize and locate Refreshing progress bar
        mRefreshBar = (ProgressBar) v.findViewById(R.id.refres_bar);

        mListEmptyView = (TextView) v.findViewById(R.id.empty_text);

        // Initialize and set adapter for feed list view.
        mPersonsFeedList = (ListView) v.findViewById(R.id.persons_feed);
        mPersonsFeedList.setEmptyView(mListEmptyView);
        mPersonsFeedList.setAdapter(mPersonsAdapter);

        // Set scrolling listener to load new part of data into list when last element is shown
        mPersonsFeedList.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                // Load data if available and if grid scrolled to the end
                boolean isEnd = firstVisibleItem + visibleItemCount >= totalItemCount &&
                        mPageToLoad <= mTotalPages - 1 && !mIsRefreshing;
                if (isEnd) {
                    loadUsers(mPageToLoad);
                }
            }
        });

        return v;
    }

    @Override
    public void onDestroy() {

        // Close database connection
        mDb.close();
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(Consts.SAVED_STATE_PAGE_TO_LOAD, mPageToLoad);
    }

    /**
     * Start {@link xyz.spacefox.rateme.MatchActivity} to show matched person.
     *
     * @param userId id of user that matched
     */
    private void performMatch(int userId) {
        Intent matchIntent = new Intent(getActivity(), MatchActivity.class);
        matchIntent.putExtra(Consts.USER_ID_INTENT_EXTRA, userId);
        startActivity(matchIntent);
    }

    /**
     * Asynchronously load users from API and saves loaded data to a local database
     *
     * @param page number of page to load
     */
    private void loadUsers(final int page) {

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                // Start refresh
                setRefreshing(true);
            }

            @Override
            protected Void doInBackground(Void... params) {
                API.INSTANCE.getPersons(page, new PersonsExtendedCallback() {
                    @Override
                    public void onResult(String persons) {

                        // Change page to load to next one
                        mPageToLoad += 1;

                        // Save loaded data to a database
                        mDb.addUsersList(ApiParser.getInstance().parseUserList(persons));

                        // Force loader to update cursor
                        forceReload();
                    }

                    @Override
                    public void onFail(String reason) {
                        Log.w(LOG_TAG, reason);
                    }
                });
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                setRefreshing(false);
            }
        }.execute();

    }

    /**
     * Sets refresh indicator according to refresh state and show/hide progress bar.
     * @param isRefreshing refreshing state to set.
     */
    private void setRefreshing(Boolean isRefreshing) {
        mIsRefreshing = isRefreshing;

        if (isRefreshing) {
            mRefreshBar.setVisibility(View.VISIBLE);
            mListEmptyView.setText(getString(R.string.empty_loading));
        } else {
            mRefreshBar.setVisibility(View.GONE);
            mListEmptyView.setText(getString(R.string.empty_no_data));
        }
    }

    /**
     * Forces activity's loader to load new data immediately
     */
    private void forceReload() {

        Loader mLoader = getActivity().getSupportLoaderManager()
                .getLoader(Consts.PERSONS_FEED_LOADER_ID);
        if (mLoader != null) {
            mLoader.forceLoad();
        }
    }

    /**
     * Swap cursor to get actual data from database
     *
     * @param c new cursor
     */
    public void swapCursor(Cursor c) {
        if (mPersonsAdapter != null) {
            mPersonsAdapter.swapCursor(c);
        }

        // If no actual data to show and all pages are loaded - go back to Refresh Activity
        if (c.getCount() < 1 && mPageToLoad != 0 && mPageToLoad > mTotalPages-1 && !mIsRefreshing) {
            Intent refreshIntent = new Intent(getContext(), RefreshActivity.class);
            startActivity(refreshIntent);
            getActivity().finish();
        }
    }

}