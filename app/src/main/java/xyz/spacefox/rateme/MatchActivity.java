package xyz.spacefox.rateme;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import xyz.spacefox.rateme.utils.Consts;
import xyz.spacefox.rateme.utils.Database;


/**
 * MatchActivity: shows matched person photo and "Go back" button.
 * Back button finalizes activity.
 */
public class MatchActivity extends AppCompatActivity {

    // Activity Toolbar
    private Toolbar mToolbar;

    // Database helper instance
    private Database mDb;

    // Id of user from intent
    private int matchedUserId;

    // Matched peprson photo ad "back" button
    private ImageView mMatchedPhoto;
    private Button mBackButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_match);

        initToolbar();

        // Locate and init activity views
        mMatchedPhoto = (ImageView) findViewById(R.id.matched_photo);
        mBackButton = (Button) findViewById(R.id.btn_back);

        // Initialize and open database
        mDb = new Database(this);
        mDb.open();

        // Getting matched user id from intent
        matchedUserId = getIntent().getIntExtra(Consts.USER_ID_INTENT_EXTRA, -1);

        // Set "back" button listener
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        // Set matched person photo
        if (matchedUserId != -1) {
            Picasso.with(this).load(mDb.getUserById(matchedUserId).getPhoto()).into(mMatchedPhoto);
        }


    }

    @Override
    protected void onDestroy() {

        // Close database when activity destroyed.
        mDb.close();
        super.onDestroy();
    }

    /** Initializes the toolbar. */
    private void initToolbar(){
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(getString(R.string.match_title));
        setSupportActionBar(mToolbar);
    }
}
