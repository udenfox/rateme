package xyz.spacefox.rateme.interfaces;

/**
 * Listener interface to handle "Like" or "Dislike" buttons press on each person in feed.
 */
public interface OnUserRateListener {

    /**
     * Fired when "Like" button pressed
     * @param userId Id of user that got like
     */
    void onLike(int userId);

    /**
     * Fired when "Dislike" button pressed
     * @param userId Id of user that got dislike
     */
    void onDislike(int userId);
}
