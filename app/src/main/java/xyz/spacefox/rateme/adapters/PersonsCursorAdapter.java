package xyz.spacefox.rateme.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import xyz.spacefox.rateme.R;
import xyz.spacefox.rateme.interfaces.OnUserRateListener;
import xyz.spacefox.rateme.ui.SquareImageView;
import xyz.spacefox.rateme.utils.Consts;

/**
 * Subclass of {@link android.widget.CursorAdapter} which provides cursor adapter for custom list
 * item and dataset.
 *
 * Can notify about item buttons pressed by user with {@link xyz.spacefox.rateme.interfaces.OnUserRateListener}
 */
public class PersonsCursorAdapter extends CursorAdapter {

    // Layout inflater
    private LayoutInflater mInflater;

    // Listener to notify about buttons pressed
    private OnUserRateListener mRateListener;


    public PersonsCursorAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);

        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return mInflater.inflate(R.layout.persons_feed_item, viewGroup, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        SquareImageView personImage = (SquareImageView) view.findViewById(R.id.person_photo);
        ImageView likeIndicator = (ImageView) view.findViewById(R.id.like_indicator);
        Button likeButton = (Button) view.findViewById(R.id.btn_like);
        Button dislikeButton = (Button) view.findViewById(R.id.btn_dislike);

        String photoUrl = cursor.getString(cursor.getColumnIndex(Consts.DB_PERSONS_COL_PHOTO));
        String status = cursor.getString(cursor.getColumnIndex(Consts.DB_PERSONS_COL_STATUS));
        final int userId = cursor.getInt(cursor.getColumnIndex(Consts.DB_PERSONS_COL_ID));

        Picasso.with(context).load(photoUrl).into(personImage);

        // Show or hide liked status indicator
        if (status.equals(Consts.STATUS_LIKE)){
            likeIndicator.setVisibility(View.VISIBLE);
        } else {
            likeIndicator.setVisibility(View.GONE);
        }

        // Set listeners for buttons
        likeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mRateListener != null) {
                    mRateListener.onLike(userId);
                }

            }
        });

        dislikeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mRateListener != null) {
                    mRateListener.onDislike(userId);
                }

            }
        });

    }

    /**
     * Sets listener to notify about buttons .
     * @param listener listener to set.
     */
    public void setOnRateListener(OnUserRateListener listener){
        this.mRateListener = listener;

    }

}
