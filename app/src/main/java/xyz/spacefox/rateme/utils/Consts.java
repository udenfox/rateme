package xyz.spacefox.rateme.utils;

/**
 * Class that contains all project constants
 */
public class Consts {

    public final static int USERS_TOTAL = 20;
    public final static int USERS_PER_PAGE = 10;

    public final static int MATCH_NOTIFY_ID = 1;
    public final static int REMOVE_NOTIFY_ID = 2;

    public final static int PERSONS_FEED_LOADER_ID = 0;

    public final static String PREFS_NAME = "xyz.spacefox.rateme.PREFS";
    public final static String PREFS_FIRSTRUN = "firstrun";

    public final static String SAVED_STATE_PAGE_TO_LOAD = "page_to_load";
    public final static String SAVED_STATE_TOTAL_PAGES = "total_pages";

    public final static String UPDATE_BROADCAST_TAG = "xyz.spacefox.rateme.API_DATA_UPDATE";
    public final static String USER_ID_INTENT_EXTRA = "user_id";

    public final static String STATUS_NONE = "none";
    public final static String STATUS_LIKE = "like";
    public final static String STATUS_HATE = "hate";
    public final static String STATUS_REMOVED = "removed";

    public final static float MAP_MARKER_ANCHOR_U_V = 0.5f;


    // Database
    public final static int DB_VERSION = 3;
    public final static String DB_NAME = "rateme_db";

    // Persons table
    public final static String DB_PERSONS_TABLE_NAME = "persons";
    public final static String DB_PERSONS_COL_ID = "_id";
    public final static String DB_PERSONS_COL_LOCATION = "location";
    public final static String DB_PERSONS_COL_STATUS = "status";
    public final static String DB_PERSONS_COL_PHOTO = "photo";

    // Likes table
    public final static String DB_LIKES_TABLE_NAME = "likes";
    public final static String DB_LIKES_COL_LIKED = "is_liked";
    public final static String DB_LIKES_COL_PERSON_ID = "person_id";


    public final static String DB_CREATE_PERSONS_TABLE =
            "create table " + DB_PERSONS_TABLE_NAME + "(" +
                    DB_PERSONS_COL_ID + " integer primary key, " +
                    DB_PERSONS_COL_LOCATION + " text, " +
                    DB_PERSONS_COL_PHOTO + " text, " +
                    DB_PERSONS_COL_STATUS + " text, " +
                    " UNIQUE ( " + DB_PERSONS_COL_ID + " ) ON CONFLICT REPLACE);";

    public final static String DB_CREATE_LIKES_TABLE =
            "create table " + DB_LIKES_TABLE_NAME + "(" +
                    DB_LIKES_COL_PERSON_ID + " integer primary key, " +
                    DB_LIKES_COL_LIKED+ " integer, " +
                    " UNIQUE ( " + DB_LIKES_COL_PERSON_ID + " ) ON CONFLICT REPLACE);";

    public final static String DB_SELECT_ACTIVE =
            "SELECT * FROM " + DB_PERSONS_TABLE_NAME + " LEFT JOIN " + DB_LIKES_TABLE_NAME
                    + " ON " + DB_PERSONS_COL_ID + " = " + DB_LIKES_COL_PERSON_ID
                    + " WHERE (" + DB_PERSONS_COL_STATUS + " != '" + STATUS_REMOVED
                    + "' AND " + DB_LIKES_COL_LIKED + " IS NULL)";

    public final static String DB_DROP_PERSONS =
            "DROP TABLE IF EXISTS " + DB_PERSONS_TABLE_NAME;
    public final static String DB_DROP_LIKES =
            "DROP TABLE IF EXISTS " + DB_LIKES_TABLE_NAME;



}
