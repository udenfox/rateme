package xyz.spacefox.rateme.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.List;

import xyz.spacefox.rateme.api.model.User;

/**
 * Class to simplify interaction with the database. Contains specific methods to interact
 * with database depends on project's needs.
 */
public class Database {

    private final Context mCtx;

    // Custom DbHelper.
    private DBHelper mDBHelper;

    // Database itself
    private SQLiteDatabase mDB;

    /**
     * Constructor of database object.
     * @param ctx Context for database.
     */
    public Database(Context ctx) {
        mCtx = ctx;
    }

    /**
     * Opens a connection to database
     */
    public void open() {
        mDBHelper = new DBHelper(mCtx, Consts.DB_NAME, null, Consts.DB_VERSION);
        mDB = mDBHelper.getWritableDatabase();
    }

    /**
     * Closes the connection to database.
     */
    public void close() {
        if (mDBHelper!=null) mDBHelper.close();
    }

    /**
     * Returns cursor with all data from database
     */
    public Cursor getActiveUsers(){

//        return mDB.query(Consts.DB_PERSONS_TABLE_NAME, null,
//                Consts.DB_PERSONS_COL_STATUS + " != '" + Consts.STATUS_REMOVED +"'",
//                null, null, null,
//                Consts.DB_PERSONS_COL_ID + " DESC");
        return mDB.rawQuery(Consts.DB_SELECT_ACTIVE, null);
    }

    /**
     * Returns User with specified ID from database.
     *
     * @param id User id to return
     */
    public User getUserById(int id) {
        User usr = new User();
        Cursor cur = mDB.query(Consts.DB_PERSONS_TABLE_NAME, null,
                Consts.DB_PERSONS_COL_ID + " = " + id,
                null, null, null, null);

        if (cur.moveToFirst()) {
            usr.setId(cur.getInt(cur.getColumnIndex(Consts.DB_PERSONS_COL_ID)));
            usr.setLocation(cur.getString(cur.getColumnIndex(Consts.DB_PERSONS_COL_LOCATION)));
            usr.setStatus(cur.getString(cur.getColumnIndex(Consts.DB_PERSONS_COL_STATUS)));
            usr.setPhoto(cur.getString(cur.getColumnIndex(Consts.DB_PERSONS_COL_PHOTO)));
        }

        cur.close();

        return  usr;
    }


    /**
     * Delete all rows from persons database tables
     */
    public void clearUsers() {
        mDB.delete(Consts.DB_PERSONS_TABLE_NAME, null, null);
    }

    /**
     * Delete all rows from likes database tables
     */
    public void clearLikes() {
        mDB.delete(Consts.DB_LIKES_TABLE_NAME, null, null);
    }

    /**
     * Add user to a database
     */
    public void addUser(User user){
        ContentValues cv = new ContentValues();
        cv.put(Consts.DB_PERSONS_COL_ID, user.getId());
        cv.put(Consts.DB_PERSONS_COL_LOCATION, user.getLocation());
        cv.put(Consts.DB_PERSONS_COL_STATUS, user.getStatus());
        cv.put(Consts.DB_PERSONS_COL_PHOTO, user.getPhoto());

        mDB.insertWithOnConflict(Consts.DB_PERSONS_TABLE_NAME, null, cv, SQLiteDatabase.CONFLICT_REPLACE);
    }

    /**
     * Add users list to a database
     */
    public void addUsersList(List<User> users) {
        if (users.size() > 0){
            for(User user : users){
                addUser(user);
            }
        }
    }

    /**
     * Sets like status to a specified user.
     *
     * @param userId ID of user for which status is setting
     * @param isLiked Like status
     */
    public void setLikeStatus(int userId, Boolean isLiked){
        ContentValues cv = new ContentValues();
        cv.put(Consts.DB_LIKES_COL_PERSON_ID, userId);
        cv.put(Consts.DB_LIKES_COL_LIKED, isLiked);

        mDB.insertWithOnConflict(Consts.DB_LIKES_TABLE_NAME, null, cv, SQLiteDatabase.CONFLICT_REPLACE);
    }

    /**
     * Check like status for specified user.
     *
     * @param userId ID of user to check status.
     * @return Like status
     */
    public Boolean getLikeStatus(int userId){
        Cursor crs = mDB.query(Consts.DB_LIKES_TABLE_NAME, null,
                Consts.DB_LIKES_COL_PERSON_ID + "=" + String.valueOf(userId),
                null, null, null, null);

        if (crs.moveToFirst()) {
            Boolean liked = crs.getInt(crs.getColumnIndex(Consts.DB_LIKES_COL_LIKED)) != 0;
            crs.close();
            return liked;
        } else {
            crs.close();
            return false;
        }

    }

    /**
     * Subclass of {@link android.database.sqlite.SQLiteOpenHelper} which provides custom database helper.
     */
    private class DBHelper extends SQLiteOpenHelper {

        public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory,
                        int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(Consts.DB_CREATE_PERSONS_TABLE);
            db.execSQL(Consts.DB_CREATE_LIKES_TABLE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL(Consts.DB_DROP_PERSONS);
            db.execSQL(Consts.DB_DROP_LIKES);
            onCreate(db);
        }
    }


}
