# About App #
Test application that simulates dating and meeting social service.
People that user can like or dislike shown in a photo feed style and as a markers on the map (based on persons geolocation).

Application data provided by [API emulation library](https://github.com/psyh/testlib) that generates people and emulates their status and position updates.
User receives notification if the match is found or some person has removed from the list.

# Key features #
* Google Maps Android API used
* Android notifications used.
* Data stored locally in a SQLite database
* Android Service used to manage API updates
* Image loading and cache using Picasso.
* Third-party module integration

[**Download APK**](https://bitbucket.org/udenfox/rateme/raw/ab8bdf9d8b92e45f7daa0d55ab29134ae47f08a9/app-debug.apk)